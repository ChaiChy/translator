import dbus
import dbus.service
import threading
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop

import yaml_parser as yaml
#import can_master as can

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()

class ItemController1(dbus.service.Object):
    
    '''
		<node>
			<interface name="hs.flipper.ItemController1">
				<method name="SetItemState">
					<arg name="item" direction="in" type="s"/>
					<arg name="state" direction="in" type="i"/>
				</method>
				<sgnal name="ItemStateChanged">
					<arg name="item" type="s"/>
					<arg name="state" type="i"/>
				</sgnal>
			</interface>
		</node>
    '''
    
    @dbus.service.method('hs.flipper.ItemController1')
    def SetItemState(self, item_id: str, state: int):
        print('Item: ' + item_id + ' ; State: ' + state)		# for testing purposes
		#item = yaml.item_dict[item_id]
        #satellite_address = item['can']['id']
        #gpio_index = item['gpio']['index']
        #can.sendState(satellite_address, gpio_index, state)		# dummy 	# CANopen message senden 

    @dbus.service.signal('hs.flipper.ItemController1')
    def ItemStateChanged(self, item_id: str, state: int):
        print('sending...')
    
	


controller = ItemController1()

def run_signal(func, *args):
    def run():
        func(*args)
        return True
    return run



GLib.timeout_add(1000, run_signal(controller.ItemStateChanged,'item1', 2))

name = dbus.service.BusName("hs.flipper.ItemController1", bus)
object = ItemController1(bus, '/ItemController1')

loop = GLib.MainLoop()
loop.run()
