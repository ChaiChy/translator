import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()

def handler():
    print('Hallo')

controller = bus.get_object('hs.flipper.ItemController1',
                            '/hs/flipper/ItemController1')

controller.connect_to_signal('ItemStateChanged', handler)

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

GLib.MainLoop().run()
