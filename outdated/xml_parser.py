# XML-Datei auslesen und daraus object erstellen
import xml.etree.ElementTree as ET

#with open('ItemController.xml', 'r') as file:
#    data = file.read()
#
#root = ET.fromstring(data)

tree = ET.parse('ItemController.xml')
root = tree.getroot()
interface = root.find('interface')

interface_name = interface.attrib.get('name')
arr = interface_name.split('.')                 # 'hs.flipper.' entfernen für Dateinamen
interface_name = arr[2]

print(interface_name)

f = open(interface_name+'.py', 'w+')                # Falls Datei bereits existiert, nicht alles doppelt machen, fehlt noch
f.write('from pydbus.generic import signal\r\n')

f.write('@dbus_interface("' + interface.attrib.get('name') + '")\nclass ' + interface_name + '(object):\r\n')


for child in interface:
    
    # methods
    if child.tag == 'method':
        f.write('\tdef ' + child.attrib.get('name') + '(self')
        for para in child:
            if (para.tag == 'arg') & (para.attrib.get('direction')=='in'):
                if para.attrib.get('type')=='i':
                    type = 'int'
                elif para.attrib.get('type')=='s':
                    type = 'str'
                else:
                    pass               
                f.write(', ' + para.attrib.get('name') + ': ' + type)
        f.write('):\n\t\tpass\r\n')
        # fehlen noch Rückgabewerte bzw. out-Parameter

    # signals    
    elif child.tag == 'sgnal':
        f.write('\t@dbus_signal\n\tdef ' + child.attrib.get('name') + '(self):\n\t\treturn ')
        cnt = 0
        for para in child:
            if para.tag == 'arg': # nur out-Parameter möglich
                cnt += 1
        for para in child:
            if para.tag == 'arg':       
                cnt -= 1
                if cnt == 0:
                    f.write(para.attrib.get('name') + '\n')
                else:
                    f.write(para.attrib.get('name') + ', ')

f.close()
        