from pydbus.generic import signal
from pydbus import SessionBus					# pydbus durch sbus ersetzen
from gi.repository import GLib
from threading import Thread

import yaml_parser as yaml
#import can_master as can

class ItemController1(object):
    
    '''
		<node>
			<interface name="hs.flipper.ItemController1">
				<method name="SetItemState">
					<arg name="item" direction="in" type="s"/>
					<arg name="state" direction="in" type="i"/>
				</method>
				<sgnal name="ItemStateChanged">
					<arg name="item" type="s"/>
					<arg name="state" type="i"/>
				</sgnal>
			</interface>
		</node>
    '''
    
    ItemStateChanged = signal() # wird gesendet nach Prinzip "ItemController1.ItemStateChanged({Inhalt})"
    
    def SetItemState(self, item_id: str, state: int):
        print('Item: ' + item_id + ' ; State: ' + state)		# for testing purposes
		#item = yaml.item_dict[item_id]
        #satellite_address = item['can']['id']
        #gpio_index = item['gpio']['index']
        #can.sendState(satellite_address, gpio_index, state)		# dummy 	# CANopen message senden 



bus = SessionBus()
bus.publish("hs.flipper.ItemController1", ItemController1())
loop = GLib.MainLoop()
loop.run()
