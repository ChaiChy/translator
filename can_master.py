import canopen

network = canopen.Network()
network.connect(bustype="socketcan", channel="can0")
network.check()
 
satellit = network.add_node({satellit_adresse})           #für alle Satelliten


satellit.nmt.state = 'PRE-OPERATIONAL'         
# CA-Objekte pro engine jedes Satelliten beschreiben

satellit.nmt.state = 'OPERATIONAL'
# Enigines ausführen


# --- Methode zu verfügung stellen, die über DBus remote-ausgeführt werden kann, um Satelliten-'states' zu ändern
#
# --- dauerhaft Satelliten abfragen, um Zustandsänderungen zu bemerken (über z.B. dictdiffer vergleichen) -> bei Änderung DBus-Signal emitten
#       --> extra Thread?
#
#   Zugriff auf Satelliten erfolgt über CiA401 spezifierte Einträge in Standart object dictionary:
#       z.B. 'read = node.sdo[0x6120H].raw' 
#   für uns relevant sind '6020H', '6120H' und '6220H'
#
#   - Kontrollzugriffe: rxPDOs -> 6220H -> '0x00, 0x01, 0x02' für 'Ausführung stoppen', 'Ausfühurng starten' und 'Asufühurng pausieren'
#   - Zustandzugriffe:  txPDOs -> 6120H -> '0x00, 0x01' für 'Enigine lääuft nicht' und 'Engine läuft'
#




network.disconect()