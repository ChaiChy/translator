# Translator

## Role

This components runs on the Raspberry Pi, providing an API to interact with components of the flipper.
Through its configuration it knows how to map humanly understandable commands to CAN/CANopen payloads and vice versa.
For example, an incoming comand could like this: set item "flipper_right" to HIGH.

This component will then translate command to a message to the proper satellite address and send it.

## Configuration file

### Items 
The configuration file holds the collection of items present in the Flipper system.
Every item has to have these fields:

#### id
This field identifies this item and is used to direct commands to this item through the API.

#### direction
Can have the values "input" / "output", indicating the data direction from the perspective of the satellite.
Connected buttons will have the direction "input", while solenoids will be "outputs", respecitvely.

#### type
Indicates the type of input / output. Currently, only "digital" is supported.
"analog" is also planned.

#### can
This object contains CAN-related information.
Its element `ìd` contains the ID of the responsible satellite.

#### gpio (optional)
This object contains gpio-related information, like the input / output index stored in the `index` field.

### Structure

The items file can, at its root, just contain a list of objects, each implementing every required field:
```
- id: flipper_right
  type: digital
  direction: output
  can:
    id: 0x123
  gpio:
    index: 1
- id: flipper_left
  type: digital
  direction: output
  can:
    id: 0x123
  gpio:
    index: 2
```

The item object can also be embedded in many dimensions of lists to allow for grouping.
Additionally, all fields set on a group will be applied to its members, unless overwritten.
This allows for structurization and spares redundant data.
The following example is functionally identical to the example above, but shorter,
since both flipper items have the same type, direction and CAN properties due to being controlled by the same satellite:
```
items:
  flippers:
    type: digital
    direction: output
    can:
      id: 0x123
    items:
      - id: flipper_right
        gpio:
          index: 1
      - id: flipper_left
        gpio:
          index: 2
```