import yaml

# Dictionaries
item_dict = {}
can_dict = {}

with open('items.yaml', 'r') as file:
    config = yaml.safe_load(file)

def handle_item(item):
    item_dict[item['id']] = item
    can_dict[f'{item["can"]["id"]}-gpio-{item["gpio"]["index"]}'] = item

def parse_object(value, state={}):
    pass
    if isinstance(value, list):
        for object in value:
            parse_object(object, state)
        return
    if isinstance(value, dict):
        for key, val in value.items():
            if key in ['can']:
                state |= value
                continue
            if key == 'id':
                handle_item(value | state)
                continue
            parse_object(val, value | state)
        return


parse_object(config['items'])

print(item_dict['flipper_right'])
