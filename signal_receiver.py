import dasbus
from dasbus.loop import EventLoop
from dasbus.connection import SessionMessageBus

bus = SessionMessageBus()

def callback(item_id, state):
    """The callback of the DBus signal NotificationClosed."""
    print(f'Received signal {item_id}, {state}')


if __name__ == "__main__":

    proxy = bus.get_proxy(
        "hs.flipper.ItemController",
        "/hs/flipper/ItemController"
    )
    # Connect the callback to the DBus signal NotificationClosed.
    proxy.StateChanged.connect(callback)

    # Start the event loop.
    loop = EventLoop()
    loop.run()