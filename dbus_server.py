from dasbus.connection import SessionMessageBus
from dasbus.loop import EventLoop
import dasbus.server.interface 
import dasbus.typing
import dasbus.signal
from gi.repository import GLib
from dasbus.xml import XMLGenerator
import time

import yaml_parser as yaml
#import can_master as can

bus = SessionMessageBus()


@dasbus.server.interface.dbus_interface("hs.flipper.ItemController")
class ItemController(object):

    StateChanged = dasbus.server.interface.dbus_signal()

    def ChangeState(self, item_id: dasbus.typing.Str, state: dasbus.typing.Int):
        print(f'Gesendet : {item_id} -> {state}')          #testing
        #item = yaml.item_dict[item_id]
        #satellite_address = item['can']['id']
        #gpio_index = item['gpio']['index']
        #can.sendState(satellite_address, gpio_index, state)		# dummy 	# CANopen message senden 
    
    @dasbus.server.interface.dbus_signal
    def StateChanged(self, item_id: dasbus.typing.Str, state: dasbus.typing.Int):
        pass
        


def run_signal(func, *args):
    def run():
        func(*args)
        return True
    return run


if __name__ == "__main__":
    # Print the generated XML specification.
    print(XMLGenerator.prettify_xml(ItemController.__dbus_xml__))

    try:
        # Create an instance of the class ItemController.
        item_controller = ItemController()

        # Publish the instance at /hs/flipper/ItemController.
        bus.publish_object("/hs/flipper/ItemController", item_controller)

        # Register the service name hs.flipper.ItemController
        bus.register_service("hs.flipper.ItemController")

        # Start the event loop.
        GLib.timeout_add(2000, run_signal(item_controller.StateChanged.emit, "item0", 2 ))      #testing purposes: signal emitting
        loop = GLib.MainLoop()
        loop.run()

    finally:
        # Unregister the DBus service and objects.
        bus.disconnect()